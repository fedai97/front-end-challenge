import * as POSTS from "../constants/posts";

export function failure(error) {
  return {
    type: POSTS.FAILURE,
    error,
  }
}

export function loadData() {
  return {
    type: POSTS.LOAD_POSTS_DATA
  }
}

export function loadDataSuccess(data) {
  return {
    type: POSTS.LOAD_POSTS_DATA_SUCCESS,
    data,
  }
}
