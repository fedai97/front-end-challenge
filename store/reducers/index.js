import * as POSTS from '../constants/posts';

const initState = {
  error: false,
  posts: null
};

function reducer(state = initState, action) {
  switch (action.type) {
    case POSTS.FAILURE:
      return {
        ...state,
        ...{
          error: action.error
        },
      };

    case POSTS.LOAD_POSTS_DATA_SUCCESS:
      return {
        ...state,
        ...{
          posts: action.data
        },
      };

    default:
      return state;
  }
}

export default reducer;

