import { all, call, delay, put, take, takeLatest } from 'redux-saga/effects';
import 'isomorphic-unfetch';

import {failure, loadDataSuccess } from '../store/actions/posts';
import * as POSTS from "../store/constants/posts";

function* loadDataSaga() {
  try {
    const res = yield fetch('https://simple-blog-api.crew.red/posts');
    const data = yield res.json();
    yield put(loadDataSuccess(data));
  } catch (err) {
    yield put(failure(err));
  }

}

function* rootSaga() {
  yield all([
    takeLatest(POSTS.LOAD_POSTS_DATA, loadDataSaga),
  ])
}

export default rootSaga
