import React from 'react';

const PostItem = ({item}) => (
  <div className="col-lg-4 col-md-6 mb-4">
    <p>===============================</p>
    <p>ID: {item.id}</p>
    <p>TITLE: {item.title}</p>
    <p>BODY: {item.body}</p>
    <p>===============================</p>
  </div>
);

PostItem.defaultProps = {
  item: {}
};

export default PostItem;
