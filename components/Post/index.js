import { connect } from 'react-redux'
import React from 'react';
import PostItem from './PostItem';


function Post({ error, posts }) {
  return (
    <div>
      { posts && (
        <p>
          <div className="row">
            {posts.map(item => <PostItem item={item} key={item.id}/>)}
          </div>
        </p>
      )}
      { error && <p style={{ color: 'red' }}>Error: {error.message}</p> }
    </div>
  )
}

export default connect(state => state)(Post)
