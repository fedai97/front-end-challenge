import React from 'react'
import { connect } from 'react-redux'

import { loadData } from '../store/actions/posts'
import Post from '../components/Post'

class Index extends React.Component {
  static async getInitialProps(props) {
    const { store, isServer } = props.ctx;

    if (!store.getState().posts) {
      store.dispatch(loadData());
    }

    return { isServer }
  }

  render() {
    return <Post />
  }
}

export default connect()(Index)
